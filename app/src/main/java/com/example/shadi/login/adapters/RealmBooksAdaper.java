package com.example.shadi.login.adapters;

import android.content.Context;

import com.example.shadi.login.model.Book;

import io.realm.RealmResults;

/**
 * Created by shadi on 9/30/2016.
 */
public class RealmBooksAdaper extends RealmModelAdapter<Book> {
    public RealmBooksAdaper(Context context, RealmResults<Book> realmResults, boolean automaticUpdate) {
        super(context, realmResults, automaticUpdate);
    }
}
