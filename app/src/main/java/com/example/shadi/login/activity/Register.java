package com.example.shadi.login.activity;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.example.shadi.login.R;
import com.example.shadi.login.model.Person;
import com.example.shadi.login.realm.RealmController;

import io.realm.Realm;

public class Register extends AppCompatActivity {


    private EditText Name, Family, Email, Password, ConfirmPassword;
    private TextInputLayout inputLayoutName, inputLayoutFamily, inputLayoutEmail, inputLayoutPassword, inputLayoutConfirmPassword;
    private Button btnSignUp;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        inputLayoutName = (TextInputLayout) findViewById(R.id.name);
        inputLayoutFamily = (TextInputLayout) findViewById(R.id.familly);
        inputLayoutEmail = (TextInputLayout) findViewById(R.id.email);
        inputLayoutPassword = (TextInputLayout) findViewById(R.id.password);
        inputLayoutConfirmPassword = (TextInputLayout) findViewById(R.id.confirmpassword);
        Name = (EditText) findViewById(R.id.your_name);
        Family = (EditText) findViewById(R.id.your_familly);
        Email = (EditText) findViewById(R.id.your_email);
        Password = (EditText) findViewById(R.id.your_password);
        ConfirmPassword = (EditText) findViewById(R.id.your_confirmpassword);
        btnSignUp = (Button) findViewById(R.id.btnreg);

        Name.addTextChangedListener(new MyTextWatcher(Name));
        Family.addTextChangedListener(new MyTextWatcher(Family));
        Email.addTextChangedListener(new MyTextWatcher(Email));
        Password.addTextChangedListener(new MyTextWatcher(Password));
        ConfirmPassword.addTextChangedListener(new MyTextWatcher(ConfirmPassword));

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(submitForm()){


                    if(Password.getText().toString().equals(ConfirmPassword.getText().toString())) {
                        if (RealmController.with(Register.this).addUser(Name.getText().toString(), Family.getText().toString(), Email.getText().toString(), Password.getText().toString(), ConfirmPassword.getText().toString())) {
                            Toast.makeText(getApplicationContext(), "Registration was successful", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "Please choose another email", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(getApplicationContext(), "Password and confirm password are not the same", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private boolean submitForm() {
        if (!validateName()) {
            return false;
        }

        if (!validateFamily()) {
            return false;
        }

        if (!validateEmail()) {
            return false;
        }

        if (!validatePassword()) {
            return false;
        }

        if (!validateConfirmPassword()) {
            return false;
        }

        return true;
    }

    private boolean validateName() {
        if (Name.getText().toString().trim().isEmpty()) {
            inputLayoutName.setError(getString(R.string.err_msg_name));
            requestFocus(Name);
            return false;
        } else {
            inputLayoutName.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateFamily() {
        if (Family.getText().toString().trim().isEmpty()) {
            inputLayoutFamily.setError(getString(R.string.err_msg_family));
            requestFocus(Family);
            return false;
        } else {
            inputLayoutFamily.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateEmail() {
        String email = Email.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            inputLayoutEmail.setError(getString(R.string.err_msg_email));
            requestFocus(Email);
            return false;
        } else {
            inputLayoutEmail.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validatePassword() {
        if (Password.getText().toString().trim().isEmpty()) {
            inputLayoutPassword.setError(getString(R.string.err_msg_password));
            requestFocus(Password);
            return false;
        } else {
            inputLayoutPassword.setErrorEnabled(false);
        }

        return true;
    }


    private boolean validateConfirmPassword() {
        if (ConfirmPassword.getText().toString().trim().isEmpty()) {
            inputLayoutConfirmPassword.setError(getString(R.string.err_msg_Confirmpassword));
            requestFocus(ConfirmPassword);
            return false;
        } else {
            inputLayoutConfirmPassword.setErrorEnabled(false);
        }

        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.your_name:
                    validateName();
                    break;
                case R.id.your_familly:
                    validateFamily();
                    break;
                case R.id.your_email:
                    validateEmail();
                    break;
                case R.id.your_password:
                    validatePassword();
                    break;
                case R.id.your_confirmpassword:
                    validateConfirmPassword();
                    break;
            }
        }
    }
}
