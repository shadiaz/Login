package com.example.shadi.login.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by shadi on 9/26/2016.
 */
public class Person extends RealmObject {

    private String Name, Family ,ConfirmPassword;
    @PrimaryKey
    private String Email;
    private String Password;

    public void setName(String name) {
        Name = name;
    }

    public void setFamily(String family) {
        Family = family;
    }

    public void setConfirmPassword(String confirmPassword) {
        ConfirmPassword = confirmPassword;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getName() {
        return Name;
    }

    public String getFamily() {
        return Family;
    }

    public String getConfirmPassword() {
        return ConfirmPassword;
    }

    public String getEmail() {
        return Email;
    }

    public String getPassword() {
        return Password;
    }
}
