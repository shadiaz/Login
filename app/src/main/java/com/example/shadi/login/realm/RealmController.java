package com.example.shadi.login.realm;

import android.app.Activity;
import android.app.Application;
import android.app.Fragment;
import android.util.Log;

import com.example.shadi.login.model.Book;
import com.example.shadi.login.model.Person;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by shadi on 9/26/2016.
 */
public class RealmController  {

    private static RealmController instance;
    private final Realm realm;
    private Person person;

    public RealmController(Application application) {
        realm = Realm.getDefaultInstance();
    }

    public static RealmController with(Fragment fragment) {

        if (instance == null) {
            instance = new RealmController(fragment.getActivity().getApplication());
        }
        return instance;
    }

    public static RealmController with(Activity activity) {

        if (instance == null) {
            instance = new RealmController(activity.getApplication());
        }
        return instance;
    }

    public static RealmController with(Application application) {

        if (instance == null) {
            instance = new RealmController(application);
        }
        return instance;
    }

    public static RealmController getInstance() {

        return instance;
    }

    public Realm getRealm() {

        return realm;
    }

    //Refresh the realm istance
    public void refresh() {

        realm.refresh();
    }

    public void clearAll() {

        realm.beginTransaction();
        realm.clear(Book.class);
        realm.commitTransaction();
    }

    //find all objects in the Book.class
    public RealmResults<Book> getBooks() {

        return realm.where(Book.class).findAll();
    }

    //query a single item with the given id
    public Book getBook(String id) {

        return realm.where(Book.class).equalTo("id", id).findFirst();
    }

    //check if Book.class is empty
    public boolean hasBooks() {

        return !realm.allObjects(Book.class).isEmpty();
    }

    public boolean checkUser(String email,String password) {
        RealmResults<Person> realmObjects = realm.where(Person.class).findAll();
        for (Person myRealmObject : realmObjects) {
            if (email.equals(myRealmObject.getEmail()) && password.equals(myRealmObject.getPassword())) {

                return true;
            }
        }

        return false;
    }

    public boolean addUser(String name, String family , String email , String password ,String confirmpassword){

        RealmResults<Person> realmObjects = realm.where(Person.class).equalTo("Email", email).findAll();
            if (realmObjects.size()==0) {
                realm.beginTransaction();
                person = realm.createObject(Person.class);
                person.setName(name);
                person.setFamily(family);
                person.setEmail(email);
                person.setPassword(password);
                person.setConfirmPassword(confirmpassword);
                realm.commitTransaction();
                return true;
            }

        return false;
    }
}
